// fetch('http://localhost:3001/todos', {method:'POST', body: JSON.stringify({title: 'Second'}), headers: {'Content-Type': 'application/json'}})
// !CORS!

const express = require('express')
const app = express()
const port = 3001
const cors = require('cors')
const todos = require('./Todos')
const login = require('./Login')

app.use(cors())
app.use(express.json())

app.get('/login', (req, res) => {
    res.send(login.getUsers())
})

app.post('/signup', (req, res) => {
    res.send(login.postUser(req, req.body.username, req.body.password))
})

app.post('/login', (req, res) => {
    res.send(login.login(req.body.username, req.body.password, res))
})

app.post('/logout', (req, res) => {
    res.send(login.logout(req.body.session.sessionKey))
})

app.get('/todos', (req, res) => {
    const parse = parseFloat(req.headers.session)
    const user = login.findUser(parse)
    if (user) {
        res.send(todos.get(user))
    } else {
        res.status(401).send('Not authorized')
    }
})

app.post('/todos', (req, res) => {
    const user = login.findUser(req.body.session.sessionKey)
    if (user) {
        res.send(todos.post(req.body.title, user))
    } else {
        res.status(401).send('Not authorized')
    }
})

app.delete('/todos/:id', (req, res) => {
    const user = login.findUser(req.body.session.sessionKey)
    if (user) {
        res.send(todos.delete(req.params.id, user))
    }
})

app.get('/todos/:id', (req, res) => {
    res.send(todos.find(req.params.id))
})

app.put('/todos/:id', (req, res) => {
    res.send(todos.put(req.params.id, req.body.title))
})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})