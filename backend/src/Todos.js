class Todos {
    constructor() {
        this.todos = []
    }

    get = (reqUser) => {
        return this.todos.filter(it => it.user.sessionKey === reqUser.sessionKey)
    }

    post = (req, user) => {
        if (!this.todos.find(todo => todo.title === req) && req) {
            this.todos.push({id: Math.random(), title: req, done: false, user})
        }
        return this.todos
    }

    delete = (reqId, reqUser) => {
        this.todos = this.todos.filter(it => {
            if (it.user.sessionKey === reqUser.sessionKey) {
                return it.id !== Number(reqId)
            }
        })
        return this.todos
    }

    find = (reqId) => {
        return this.todos.find(it => it.id === Number(reqId))
    }

    put = (reqId, title) => {
        return this.todos.map(it => {
            if (it.id === Number(reqId)) {
                it.newTitle = title
                return it
            }
        })
    }
}

const todos = new Todos()

module.exports = todos