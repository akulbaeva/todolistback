class Login {
    constructor() {
        this.users = []
    }

    getUsers = () => {
        return this.users
    }

    findUser = (sessionKey) => {
        return this.users.find(it => it.sessionKey === sessionKey)
    }

    postUser = (req, reqUsername, reqPassword) => {
        const username = !this.users.find(it => it.username === reqUsername)
        if (username && req) {
            this.users.push({username: reqUsername, password: reqPassword})
        }
        return this.users
    }

    login = (reqUsername, reqPassword, res) => {
        const candidate = this.users.find(it => it.username === reqUsername && it.password === reqPassword)
        if (candidate && candidate.sessionKey) {
            return {sessionKey: candidate.sessionKey}
        } else if (candidate) {
            candidate.sessionKey = Math.random()
            return {sessionKey: candidate.sessionKey}
        }
        res.status(401).send('Not authorized')
    }

    logout = (reqSession) => {
        const candidate = this.users.find(it => it.sessionKey === reqSession)
        if (candidate) {
            candidate.sessionKey = reqSession
            return {status: 'ok'}
        }
        return this.users
    }
}

const login = new Login()

module.exports = login