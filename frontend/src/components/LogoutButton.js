const LogoutButton = ({logoutBtn}) => {

    return (
        <button onClick={logoutBtn}>Log Out</button>
    )

}

export default LogoutButton