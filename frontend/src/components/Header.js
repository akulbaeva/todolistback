import todoStyle from "./Todos/Todo.module.css";
import React from "react";

const Header = () => {
    return (
        <header className="App-header">
            <h1 className={todoStyle.todoStyle}>todos</h1>
        </header>
    )
}
export default Header