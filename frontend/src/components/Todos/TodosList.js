import React from "react";
import Todo from "./Todo";

const todosUrl = 'http://localhost:3001/todos'

const TodosList = ({todos, setTodos, session}) => {

    const removeTodo = (id) => {
        fetch(`${todosUrl}/${id}`, {
            method: 'delete',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({session})
        }).then(it => it.json())
            .then(() => setTodos(todos.filter(it => it.id !== id)))
    }

    return (
        <div>
            {todos.map((todo, index) =>
                <Todo key={index}
                      index={index}
                      todo={todo}
                      removeTodo={removeTodo}
                />
            )}
        </div>
    )
}
export default TodosList