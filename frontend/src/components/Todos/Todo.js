import React from "react";
import '../../App.css';
import todoStyle from './Todo.module.css'

const Todo = ({todo, removeTodo}) => {


    return (
        <div className={todoStyle.todoList}>
            <div>
                {todo.title}
                <button onClick={(e) => {
                    removeTodo(todo.id)
                    e.stopPropagation()
                }
                } className={todoStyle.deleteButton}> X
                </button>
            </div>
        </div>


    )
}

export default Todo