import React, {useState} from "react";

const InputForm = ({setTodos, session}) => {
    const [value, setValue] = useState('');

    const addTodo = () => {
        fetch('http://localhost:3001/todos/', {
            method: 'post',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({title: value, session})
        }).then(it => it.json())
            .then(fetched => setTodos(fetched))
        setValue('')
    }

    const enterKey = (e) => {
        if (e.keyCode === 13) {
            e.preventDefault();
            addTodo()
            setValue('')
        }
    }

    return (
        <div>
            <input type="text" placeholder="TO DO"
                   onChange={(e) => setValue(e.target.value)}
                   value={value}
                   onKeyDown={enterKey}
            />
            <button onClick={addTodo}>Add</button>
        </div>
    )
}

export default InputForm