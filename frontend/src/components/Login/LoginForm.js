import todoStyle from "../Todos/Todo.module.css";
import React from "react";

const LoginForm = (props) => {
    return (
        <form>
            <div>
                <input type="username"
                       name="username"
                       placeholder="Enter username"
                       value={props.username}
                       onChange={e => props.setUsername(e.target.value)}
                       required/>
            </div>
            <div>
                <input type="password"
                       name="password"
                       placeholder="Enter password"
                       value={props.password}
                       onChange={e => props.setPassword(e.target.value)}
                       required/>
            </div>

            <div className={todoStyle.buttonStyle}>
                <div>
                    <button onClick={props.login}>Log In</button>
                </div>
                <div style={{paddingTop: "10px"}}>
                    <button onClick={props.addUser}>Sign Up</button>
                </div>
            </div>

        </form>
    )

}

export default LoginForm