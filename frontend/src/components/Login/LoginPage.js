import '../../App.css'
import React, {useState} from "react"
import todoStyle from '../Todos/Todo.module.css'
import LoginForm from "./LoginForm";

const LoginPage = ({loginBtn}) => {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')

    const addUser = (e) => {
        e.preventDefault()
        fetch('http://localhost:3001/signup', {
            method: 'post',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                username: username,
                password: password
            })
        }).then(it => it.json())
        setPassword('')
        setUsername('')
    }

    return (
        <div className={todoStyle.divStyle}>
            <LoginForm
                username={username}
                setUsername={setUsername}
                password={password}
                setPassword={setPassword}
                login={(e) => loginBtn(e, username, password)}
                addUser={addUser}
            />
        </div>
    )
}

export default LoginPage