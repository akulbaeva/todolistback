import React, {useEffect, useState} from 'react';
import './App.css';
import todoStyle from './components/Todos/Todo.module.css'
import LogoutButton from "./components/LogoutButton";
import LoginPage from "./components/Login/LoginPage";
import InputForm from "./components/InputForm";
import Header from "./components/Header";
import TodosList from "./components/Todos/TodosList";

const todosUrl = 'http://localhost:3001/todos'

const App = () => {
    const [todos, setTodos] = useState([])
    const [session, setSession] = useState(JSON.parse(localStorage.getItem('sessionKey')))

    useEffect(() => {
            console.log(session)
            fetch(todosUrl, {
                headers: {'Content-Type': 'application/json', session: session.sessionKey},
            })
                .then(it => it.json())
                .then(fetched => setTodos(fetched))
        },
        // eslint-disable-next-line
        [])

    const login = (e, username, password) => {
        e.preventDefault()
        fetch('http://localhost:3001/login', {
            method: 'post',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                username: username,
                password: password
            })
        }).then(it => it.json())
            .then((it) => {
                localStorage.setItem('sessionKey', JSON.stringify(it))
                setSession(it)
            })
    }

    const logoutBtn = () => {
        fetch('http://localhost:3001/logout', {
            method: 'post',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                session
            })
        }).then(it => it.json())
            .then(() => {
                localStorage.removeItem('sessionKey')
                setSession('')
            })

    }

    return (
        <div className="App ">
            {session ?
                (<div>
                    <Header/>
                    <div className={todoStyle.container}>
                        <InputForm setTodos={setTodos} session={session}/>
                        <TodosList todos={todos} setTodos={setTodos} session={session}/>
                        <LogoutButton logoutBtn={logoutBtn}/>
                    </div>
                </div>)
                : (<LoginPage loginBtn={login}/>)}
        </div>
    );

}

export default App;
